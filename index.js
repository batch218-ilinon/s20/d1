// console.log("Hello World");
/*
	- A while loop takes in an expression/condition
	- Expressions are any unit of code that can evaluate to a value
	- If the condition evaluates to true, the statements inside the block will be exevcuted.
*/

let count = 5;

while(count !== 0){
	console.log("Count: " + count);
	count--;
};

// [SECTION] Do while

/*let number = Number(prompt("Give me a number: "));
// The "Number" function works similar to the "parseInt" function.

do {
	console.log("Number: " + number);
	number++;
} while(number < 10);*/

// [SECTION] For loop

let myName = "Alex";
let vowelCount = 0;
for(let i=0; i<myName.length; i++){
    if(
        myName[i].toLowerCase() == "a" ||
        myName[i].toLowerCase() == "e" ||
        myName[i].toLowerCase() == "i" ||
        myName[i].toLowerCase() == "o" ||
        myName[i].toLowerCase() == "u" 
    ){
        console.log(3);
        vowelCount ++;
    }
    else{
        console.log(myName[i]);
    }
}

console.log("Vowels: " + vowelCount);
